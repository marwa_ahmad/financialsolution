﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Helpers
{
    public static class Extensions
    {
        public static bool IsNotEmpty<T>(this IEnumerable<T> enumerable)
        {
            return (enumerable != null && enumerable.Any());
        }
        public static bool IsEmpty<T>(this IEnumerable<T> enumerable)
        {
            return (enumerable == null || enumerable.Any() == false);
        }        
    }
}
