﻿using System.Linq;

namespace Common.Helpers
{
    public class Utilities
    {
        public static bool IsValidInputStrings(params string[] input)
        {
            if (input.IsEmpty()) return false;
            for (var i = 0; i < input.Count(); i++)
            {
                if (string.IsNullOrWhiteSpace(input[i]))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
