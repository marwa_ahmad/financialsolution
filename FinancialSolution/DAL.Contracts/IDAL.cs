﻿using System.Collections.Generic;

namespace DAL.Contracts
{
    public interface IDAL
    {
        bool IsLoaded();
        void Load();
        int GetPracticesCountInRegion(string region);//1. How many practices are in London?

        int GetAvgActualCost(string medicineName); //2. What was the average actual cost of all peppermint oil prescriptions?

        Dictionary<string, int> GetPostCodesWithHighestActualSpend(int level = 5, string havingKey = "actualspend");

    }
}
