﻿using System.Collections.Generic;
using Bll.Contracts;
using Common.Helpers;
using DAL.Contracts;

namespace Bll
{
    public class BLL : IBLL
    {
        private readonly IDAL _dalObject;
        public BLL(IDAL dalObject)
        {
            _dalObject = dalObject;
            if(_dalObject.IsLoaded() == false) _dalObject.Load();
        }
        public int GetNumberOfPractices(string region)
        {
            return Utilities.IsValidInputStrings(region) == false ? 0 : _dalObject.GetPracticesCountInRegion(region);
        }

        public int GetAvgActualCost(string medicineName)
        {
            return Utilities.IsValidInputStrings(medicineName) == false ? 0 : _dalObject.GetAvgActualCost(medicineName);       
        }

        public Dictionary<string, int> GetPostCodesWithHighestActualSpend(int level = 5)
        {
            return _dalObject.GetPostCodesWithHighestActualSpend(level);
        }

        public int GetAveragePricePerprescription(string region, string medicine)
        {
            throw new System.NotImplementedException();
        }

        public int GetVariationFromNationalMean_AveragePricePerprescription(string region, string medicine)
        {
            throw new System.NotImplementedException();
        }
    }
}
