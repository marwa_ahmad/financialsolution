﻿using System.Collections.Generic;

namespace Bll.Contracts
{
    public interface IBLL
    {

        //1. How many practices are in London?
        int GetNumberOfPractices(string region);

        //2. What was the average actual cost of all peppermint oil prescriptions?
        int GetAvgActualCost(string medicineName);

        //3. Which 5 post codes have the highest actual spend, and how much did each spend in total?
        Dictionary<string, int> GetPostCodesWithHighestActualSpend(int level = 5);

        //4For each region of England (North East, South West, London, etc.):
        //a. What was the average price per prescription of Flucloxacillin (excluding Co-Fluampicil)?
        int GetAveragePricePerprescription(string region, string medicine);

        //b. How much did this vary from the national mean?
        int GetVariationFromNationalMean_AveragePricePerprescription(string region, string medicine);
    }
}
