﻿using System;
using Bll;
using Bll.Contracts;
using DAL.Contracts;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {            
            var practicesFilePath = @"D:\Projects\FinancialSolution\InputTestData\practices.CSV";
            var prescriptionsFilePath = @"D:\Projects\FinancialSolution\InputTestData\prescriptions.CSV";

            IDAL dal = new DAL.DAL(practicesFilePath, prescriptionsFilePath);
            IBLL bll = new BLL(dal);
            
            // 1. How many practices are in London?
            //var count = bll.GetNumberOfPractices("London");


            //IDAL dal = new DAL.DAL(prescriptionsFilePath);

            //2. What was the average actual cost of all peppermint oil prescriptions?
            //var avgCost = bll.GetAvgActualCost("peppermint oil");//("BNF NAME")//ACT COST

            //3. Which 5 post codes(ZipCode) have the highest actual spend, and how much did each spend in total?
            var result = bll.GetPostCodesWithHighestActualSpend();
            Console.WriteLine("done");
        }
    }
}
