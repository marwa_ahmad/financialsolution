﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Common.Helpers;
using DAL.Contracts;
using GenericParsing;

namespace DAL
{
    public class DAL : IDAL
    {        
        private DataTable _dataTable;
        private DataTable _dataTablePrescription;
        private readonly string _filePath;
        private readonly string _filePathPrescription;
        private bool _isLoaded;
        private int _maxRowCount = 0;

        public DAL()
        {
            _dataTable = new DataTable();

        }
        private void Load(string filePath, ref DataTable dataTable, int maxRows, int skipStartingDataRows)
        {
            using (var parser = new GenericParserAdapter())
            {
                parser.SetDataSource(filePath);

                parser.ColumnDelimiter = ',';
                parser.FirstRowHasHeader = true;
                parser.SkipStartingDataRows = skipStartingDataRows;
                parser.MaxBufferSize = 4096;
                parser.MaxRows = maxRows;
                parser.TextQualifier = '\"';
                parser.SkipEmptyRows = true;
                parser.TrimResults = true;
                parser.StripControlChars = true;                
                dataTable = parser.GetDataTable();
            }
        }
        private void LoadPrescriptions(int maxRows, int skipStartingDataRows)
        {
            Load(_filePathPrescription, ref _dataTablePrescription, maxRows, skipStartingDataRows);
        }
        private void LoadPractices()
        {
            Load(_filePath, ref _dataTable, 0, 0);
        }
        public bool IsLoaded()
        {
            return _isLoaded;
        }
        public DAL(string filepath, string prescriptionFilePath)
        {
            _filePath = filepath;
            _filePathPrescription = prescriptionFilePath;
        }
        public void Load()
        {
            _isLoaded = false;
            var taskPractices = Task.Factory.StartNew(LoadPractices);            
            Task.WaitAll(new [] {taskPractices});
            _isLoaded = true;
        }

        //example: // How many practices are in London?
        private int GetNumberColumnRows(DataTable dataTable, string columnName, string conditionColumnName, string coditionColumnValue, Conditions condition)
        {
            IEnumerable<DataRow> result;
            switch (condition)
            {
                case Conditions.Equal:
                    result = from row in dataTable.AsEnumerable()
                             where (string.IsNullOrWhiteSpace(row.Field<string>(columnName)) == false)
                            && (string.Compare(row.Field<string>(conditionColumnName),(coditionColumnValue), StringComparison.OrdinalIgnoreCase) == 0)
                            select row;
                break;
                default:
                throw new NotSupportedException();
            }            
            return result.IsEmpty() ? 0 : result.Count();
        }

        public int GetPracticesCountInRegion(string region)
        {
            return GetNumberColumnRows(_dataTable, "PracticeCode", "Region", region, Conditions.Equal);
        }

        public int GetAvgActualCost(string medicineName)
        {
            float avg = 0;
            float sum = 0;
            var iOP = 0;
            string medicineColumnName = "BNF NAME";
            string actualCostColumnName = "ACT COST";

            Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < (1048576/2000); i++)
                {
                    _dataTablePrescription = null;

                    LoadPrescriptions(2000, i + (i*2000));
                    if (_dataTablePrescription == null) continue;
                    sum = 0;
                    var result =    from row in _dataTablePrescription.AsEnumerable()
                                    where (string.IsNullOrEmpty(row.Field<string>(medicineColumnName)) == false)
                                            && string.Compare(row.Field<string>(medicineColumnName), medicineName, StringComparison.OrdinalIgnoreCase) == 0
                                            && (string.IsNullOrWhiteSpace(row.Field<string>(actualCostColumnName)) == false)
                                    select row;
                    if (result.IsEmpty() == false)
                    {                        
                        foreach (var dataRow in result)
                        {
                            var actCost = dataRow["ACT COST"];
                            if (actCost != null)
                            {
                                var actCostFloat = Single.Parse((string) actCost,
                                    CultureInfo.InvariantCulture.NumberFormat);
                                sum += actCostFloat;
                            }
                        }
                        avg += (sum/result.Count());
                        iOP++;
                    }
                }
            }).Wait();
            avg = (avg == 0 || iOP == 0) ? 0: avg/iOP;

            return Convert.ToInt32(avg);
        }

        //List<Dictionary<string, int>> GetHighestPostCodes(int level = 5, string havingKey = "actualspend");
        /*
                3. Which 5 post codes have the highest actual spend, and how much did each spend in total?          
        select zipcode --5records
        from _tablePractices

        from _prescriptionsTable
        order by t.sum(actualspend)
        group by PRACTICE
        select records).take(5)
        */
        public Dictionary<string, int> GetPostCodesWithHighestActualSpend(int level = 5, string havingKey = "ACT COST")
        {
            var highestPracticesActualSpend = new Dictionary<string, long>();
            Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < (1048576 / 5000); i++)
                {
                    _dataTablePrescription = null;

                    LoadPrescriptions(5000, i + (i * 5000));
                    if (_dataTablePrescription == null) continue;
                    //sum = 0;
                    var result = from row in _dataTablePrescription.AsEnumerable()                                 
                                 group row by row.Field<int>("PRACTICE") into pracs
                                 orderby pracs.Sum(a => (Single.Parse(a.Field<string>(havingKey), CultureInfo.InvariantCulture.NumberFormat)))
                                 where pracs.IsNotEmpty()
                                 select pracs.Take(5);
                    if (result.IsEmpty()) continue;
                    
                }
            }).Wait();
            return null;            
        }

    }

    public enum Conditions
    {
        GreaterThan,
        LessThan,
        Equal
    }
}
